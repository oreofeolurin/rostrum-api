import {Module} from "@nestjs/common";
import {MongodbHandle} from "./connections/mongodb-handle";

@Module({
    components: [MongodbHandle],
    exports: [MongodbHandle]
})
export class CoreModule{}
