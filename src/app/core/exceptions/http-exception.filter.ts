import {Catch, ExceptionFilter, HttpException} from "@nestjs/common";


@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {

    public catch(exception: HttpException, response) {
        const status = exception.getStatus();

        response.status(status).json({
            code: status,
            message: exception.getResponse(),
        });
    }
}