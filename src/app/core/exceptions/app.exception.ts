import {HttpStatus} from "@nestjs/common";
import {CustomException} from "./custom-exception";
import {AppStatus} from "../helpers/enums";

export class AppException extends CustomException{

    constructor(err: any, code: number, status: number = HttpStatus.INTERNAL_SERVER_ERROR,  message = "App Exception") {
        super(err, code, status, message);
    }

    public static get USER_EXIST(){
        return new this("User Exist",AppStatus.USER_EXIST, HttpStatus.BAD_REQUEST);
    }

    public static get UNKNOWN_USER(){
        return new this("Unknown User",AppStatus.UNKNOWN_USER, HttpStatus.BAD_REQUEST);
    }


    public static get INVALID_PASSWORD(){
        return new this("Invalid Password", AppStatus.INVALID_PASSWORD, HttpStatus.UNAUTHORIZED);
    }

    public static get INVALID_TOKEN(){
        return new this("Invalid Token", AppStatus.INVALID_TOKEN, HttpStatus.UNAUTHORIZED);
    }
}
