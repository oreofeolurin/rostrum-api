import {Catch, ExceptionFilter} from "@nestjs/common";
import {CustomException} from "./custom-exception";

@Catch(CustomException)
export class CustomExceptionFilter implements ExceptionFilter {

    public catch(exception: CustomException , response) {

        response.status(exception.getStatus()).json({
            code: exception.getCode(),
            message: exception.getMessage(),
            error: process.env.NODE_ENV == "development" ? exception.getError() : undefined
        });
    }

}
