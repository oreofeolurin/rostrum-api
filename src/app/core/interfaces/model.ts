

/**
 * Model interface, All models must implement this model
 *
 * @author  Oreofe Olurin
 * @version 0.0.1
 */

export interface Model{

    /**
     * Gets the mongoose Document
     *
     */
    getModel();

}
