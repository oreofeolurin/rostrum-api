import * as crypto from "crypto";
import * as _ from "lodash";


export class Utils {

    public static generateRandomNumber(length) : number {

        // Generate a random {length} digit code
        return Math.floor(Math.pow(10, length - 1) + (Math.random() * Math.pow(10, length - 1) * 9));

    }


    public static generateRandomID(length) : string {

        return crypto.randomBytes(length/2).toString('hex');

    }


    public static toSlug(value: string) {
        return _.kebabCase(value)
    }

    public static toSentenceCase(str) {
        return _.upperFirst(str)
    }


    /**
     * Removes all Nil values from object - undefined, null
     *
     * @param {Object} obj
     * @return {object}
     */
    public static removeNilValues(obj: object): any{
        return _.omitBy(obj, _.isNil);
    }


    /**
     * Select a space seperated keys from an object
     *
     * @param {object} obj
     * @param {string} spaceSeparatedKeys
     * @return {any}
     */
    public static selectKeys(obj: object, spaceSeparatedKeys: string){
        let keyArray = spaceSeparatedKeys.trim().split(" ");

        return _.pickBy(obj, function (value, key) {
            return _.includes(keyArray, key);
        });
    }


}
