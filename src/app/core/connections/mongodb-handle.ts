import * as mongoose from "mongoose";
(mongoose as any).Promise = global.Promise;

export class MongodbHandle{

    private connected: boolean = false;
    public handle: any;

    constructor() {

        let DBHandle = mongoose.createConnection(process.env['MONGODB_URI']);

        DBHandle
            .once('open',()=>{
                console.info("Connected to mongodb service");
                this.connected = true;
                //connector.ready();
            })
            .once('disconnected',()=>{
                if(this.connected) {
                    console.warn("Disconnected from mongodb service");
                    this.connected = false;
                    //connector.lost();
                }
                DBHandle.close();
            })
            .once('error',err => {
                console.error("Error connecting to mongodb service %s", err);
                this.connected = false;
                DBHandle.close();
                //connector.lost();
            });

        this.handle = DBHandle;
    }

    public getHandle(){
        return this.handle;
    }

    public close(){
        this.handle.close();
    }

}
