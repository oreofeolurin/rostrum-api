import * as mongoose from "mongoose";
import {Component} from "@nestjs/common";
import {User} from "./user.interface";
import {MongodbException} from "../../core/exceptions/mongodb.exception";
import {AppException} from "../../core/exceptions/app.exception";
import {UserModel} from "./user.model";
import {Repository} from "../../core/abstracts/repository";

@Component()
export class UserRepository extends Repository{

    constructor(userModel: UserModel){
        super(userModel);
    }

    public createUser(userDetails: User) {

        //Lets create a skeleton user with provided form params
        let user = new this.model(userDetails);

        //set the hashed password;
        user["setHashedPassword"](user["password"]);

        return this.save(user);
    }

    public findUserByCredential(email: string, password: string){

        return new Promise((resolve,reject) => {

            this.model
                .findOne({ email: email })
                .select("email username summary name _id password salt")
                .exec(function(err, doc: mongoose.Document) {
                    if (err)
                        return reject(new MongodbException(err));

                    if (!doc)
                        return reject(AppException.UNKNOWN_USER);

                    if (!doc['authenticate'](password))
                        return reject(AppException.INVALID_PASSWORD);

                    return resolve(doc.toJSON());
                })

        });
    }
}
