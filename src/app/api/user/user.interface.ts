export interface User  {
    _id?: string,
    email: string,
    password: string,
    name: string,
    username?: string
    summary?: string
}



