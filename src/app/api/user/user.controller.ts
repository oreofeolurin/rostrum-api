import {Controller, HttpStatus, UseGuards, Req, Param, Get} from "@nestjs/common";
import {ApiGuard} from "../api.guard";
import {User} from "./user.interface";
import {PostService} from "../post/post.service";
import {PostIdDto} from "../post/post.dto";

@Controller('/me')
@UseGuards(ApiGuard)
export class UserController {

    constructor(private readonly service: PostService) {
    }


    @Get("/post/:postId")
    public async getPost(@Req() req, @Param() params: PostIdDto) {
        const user: User = req.locals.user;

        const posts = await this.service.getPostByUser(user._id, params.postId);

        return {code: HttpStatus.OK, message: "Request Successful", body: {posts}};
    }


    @Get("/post/")
    public async getUserPosts(@Req() req) {

        const user: User = req.locals.user;

        const posts = await this.service.getUserPosts(user._id);

        return {
            code: HttpStatus.OK, message: "Request Successful", body: {posts}
        };
    }
}
