import * as crypto from 'crypto';
import * as mongoose from 'mongoose';
import {Component} from "@nestjs/common";
import {MongodbHandle} from "../../core/connections/mongodb-handle";

@Component()
export class UserModel {
    private model: mongoose.Model<mongoose.Document>;

    constructor(private readonly mongodb: MongodbHandle){
        this.registerModel();
    }


    /**
     * Gets the mongoose Document
     *
     * @return {mongoose.Model<mongoose.Document>}
     */
    public getModel(): mongoose.Model<mongoose.Document>{
        return this.model;
    }

    /**
     * Creates the mongoose model
     */
    private registerModel(){
        //create the Model
        let model = UserModel.createModel(this.mongodb.getHandle());
        this.setModel(model);
    }

    /**
     * Set the mongoose model
     */
    public setModel(model: mongoose.Model<mongoose.Document>){
        this.model = model;
    }


    /**
     * Create the mongoose model using the DBConnection
     *
     * @param DBConnection
     * @return {mongoose.Model<mongoose.Document>}
     */
    public static createModel(DBConnection : mongoose.Connection) : mongoose.Model<mongoose.Document> {

        //create schema
        let schema = this.createSchema();

        //define schema methods functions
        this.defineSchemaMethods(schema);

        //define schema static functions
        this.defineSchemaStatics(schema);

        return DBConnection.model('User',schema);


    }

    private static createSchema(): mongoose.Schema {

        let schema = new mongoose.Schema({
            username: {
                type: String,
                index:true,
                unique: true
            },
            name: {
                type: String,
                required: "FullName is Required",

            },
            email: {
                type: String,
                index:true,
                unique: true,
                required: "Email is Required",
                match: [/.+\@.+\..+/, "Please fill a valid e-mail address"],
            },
            summary: String,

            password: {
                type : String,
                validate: [
                    function (password: string) {
                        return password && password.length > 6;
                    }, 'Password should be longer']
            },
            salt: {
                type: String,
                unique: true,
            },

        },{'timestamps':true});

        schema.set('toJSON', { getters: true, virtuals: true, transform: (doc, ret) =>{
            delete ret.id;
            delete ret.password;
            delete ret.salt;
            return ret;
        }});

        return schema;

    }

    private static defineSchemaMethods(schema : mongoose.Schema) {

        //Define hashPassword method
        schema.method("hashPassword",function (password) {
            return crypto.pbkdf2Sync(password, this.salt, 10000, 64, "sha512").toString('base64');
        });

        //Define authenticate method
        schema.method("authenticate",function (password) {
            return this.password === this.hashPassword(password);
        });

        schema.method("setHashedPassword",function(password){
            //hash the password
            const buffer = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
            this.salt = buffer.toString('hex');
            this.password = this.hashPassword(password);
        })

    }

    private static defineSchemaStatics(schema  : mongoose.Schema) {

        //Define findUniqueAppID
        schema.static("ensureEmailUnique",function(emailToEnsure){
            let self = this;

            return new Promise<boolean>(function(resolve,reject){

                self.findOne({ email : emailToEnsure }, function (err, user) {
                    if(err) return reject(err);

                    if(!user) return resolve(true);

                    else return reject(new Error("RError.EMAIL_EXISTS"));
                });

            })

        })


    }

}
