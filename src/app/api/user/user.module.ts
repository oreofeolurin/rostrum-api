import {Module} from "@nestjs/common";
import {UserRepository} from "./user.repository";
import {CoreModule} from "../../core/core.module";
import {UserModel} from "./user.model";
import {UserController} from "./user.controller";
import {PostModule} from "../post/post.module";
import SEED from "./user.seed";

@Module({
    modules: [CoreModule, PostModule],
    controllers: [UserController],
    components: [UserRepository, UserModel],
    exports: [UserRepository, UserModel]
})
export class UserModule{
    constructor(private readonly repository: UserRepository) {
        this.repository.ensureSeedData(SEED);
    }
}
