export default [{
    "_id": "5a9301bdf452e12304895a10",
    "updatedAt": "2018-02-25T18:34:37.221Z",
    "createdAt": "2018-02-25T18:34:37.221Z",
    "salt": "42b6deabe3cd800831d80cfbda129e3a",
    "email": "edwinclark@mail.com",
    "username": "edwinclark",
    "password": "F1VPHv37vnPdWiHn55elPLWgdzbe9g6tZtI8ZCfMq585iW59CkyxZ1Eoa8UKjjClt2fsGb9tzta65v3qS0BNqQ==",
    "name": "Edwin Clark",
    "__v": 0
},
    {
        "_id": "5a92fedaf452e12304895a0f",
        "updatedAt": "2018-02-25T18:22:18.973Z",
        "createdAt": "2018-02-25T18:22:18.973Z",
        "salt": "532de50b7dba0d97d5f6a64982848afb",
        "email": "johndoe@mail.com",
        "username": "johndoe",
        "password": "L79Zk0BblOIObRviejCK1XzLuUZBfEiXmSptlkl0o17ULVp/CEtjCaTlnk9AmX5uXZ4tVnfVpfYIu0uwUKWpqg==",
        "name": "John Doe",
        "__v": 0
    }
]
