import {Guard, CanActivate, HttpStatus} from "@nestjs/common";
import {ExecutionContext} from "@nestjs/common/interfaces/execution-context.interface";
import {AuthUtils} from "../core/helpers/auth.utils";
import {HttpException} from "@nestjs/common";
import {AuthenticationPolicy} from "../core/helpers/enums";

@Guard()
export class ApiGuard implements CanActivate {

    constructor() {}

    async canActivate(req, context: ExecutionContext) {

        //lets first check if authorization is in headers or query
        let haystack = Object.assign(req.headers, req.query);

        if (typeof haystack["authorization"] == "undefined")
            throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);

        //now lets verify
        let verified = await this.verifyAuthorization(req, haystack["authorization"]);

        if(!verified) throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);

        return true;
    }


    private verifyAuthorization(req: Request, authString: string) {


        try {
            let authContent = authString.split(" ");
            let authType = authContent[0];
            let auth = authContent[1];

            //Ensure the right authorisation type is passed
            if (typeof AuthenticationPolicy[authType] == "undefined")
                return false;

            switch (authType){
                case AuthenticationPolicy.RJT:
                    return ApiGuard.authenticateRJT(req,auth);
            }

        } catch (err) {return false}

    }

    /**
     * Authenticate via Conduit Json-web Tokens
     * @param req
     * @param token
     * @returns {Promise<any>}
     */
    public static async authenticateRJT(req, token){

        let {user} =  await AuthUtils.decodeJwtToken(token);

        req.locals = {user};

        return true;

    }

}
