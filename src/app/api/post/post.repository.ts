import {Component} from "@nestjs/common";
import {Repository} from "../../core/abstracts/repository";
import {PostModel} from "./post.model";
import * as mongoose from "mongoose";

@Component()
export class PostRepository extends Repository{

    constructor(model: PostModel) {
        super(model);
    }


    public updateByPostId(userId: string, postId: string, updateObject: object) {
        return this.update({author: userId, postId}, { $set : updateObject});
    }

    /**
     * Gets all document in the database
     *
     * @param {Object} conditions
     * @param {string} select space separated values
     * @return {Promise<any>}
     */
    public find(conditions: object, select  = 'readTime postId summary createdAt author title category content slug'){

        return new Promise((resolve, reject) => {

            this.model
                .find(conditions)
                .select(select)
                .populate('author', 'username name summary')
                .populate('category', '_id name')
                .lean()
                .exec(function (err, docs: Array<mongoose.Document>) {

                    if (err) return reject(new Error(err));

                    return resolve(docs);

                });

        })

    }

}
