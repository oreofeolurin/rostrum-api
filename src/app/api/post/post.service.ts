import {Utils} from "../../core/helpers/utils";
import {Component} from "@nestjs/common";
import * as mongoose from "mongoose";
import {PostRepository} from "./post.repository";
import {CreatePostDto} from "./post.dto";
import {CommentRepository} from "../comment/comment.repository";
import {async} from "rxjs/scheduler/async";
import {CreateCommentDto} from "../comment/content.dto";

@Component()
export class PostService{

    constructor(private readonly repository: PostRepository,
                private readonly commentRepository: CommentRepository){}


    public async create(userId: string, body:  CreatePostDto): Promise<object>{

        //clean data
        let postDetails = {
            author: new mongoose.Types.ObjectId(userId),
            title : body.title.trim(),
            category: new mongoose.Types.ObjectId(body.category),
            content : body.content,
            summary : body.summary.trim(),
            readTime : Number(body.readTime)
        };

        postDetails['postId'] =  Utils.generateRandomID(10);
        postDetails['slug'] =  `${Utils.toSlug(postDetails.title)}-${postDetails['postId']}`;

        postDetails = Utils.removeNilValues(postDetails);


        //save to repository
        return await this.repository.create(postDetails);

    }


    public async edit(userId: string, postId: string, body:  CreatePostDto){

        //clean data
        let updateObject = {
            title : body.title.trim(),
            content : body.content,
            category: new mongoose.Types.ObjectId(body.category),
        };

        //save to repository
        return await this.repository.updateByPostId(userId, postId, updateObject);

    }

    public async delete(id: string){

        //delete from this.repository
        return await this.repository.remove({postId : id});

    }

    public async getUserPosts(id: string){
        // clean data;
        const conditions = {
            author : id.trim(),
        };

        //get from repository
        return await this.repository.find(conditions);

    }


    public async getPostByUser(userId, postId: string){
        // clean data;
        const conditions = {
            author : userId,
            postId: postId.trim()
        };

        console.log(conditions);

        //get from repository
        return await this.repository.find(conditions);

    }

    public async getLatestPosts(query){
        //clean data

        let conditions = {
            slug : query.slug && query.slug.trim(),
            postId : query.postId && query.postId.trim()
        };

        conditions =  Utils.removeNilValues(conditions);

        console.log(conditions);

        //get from repository
        return await this.repository.find(conditions);
    }


    public async getPost(postId: string) {
        //get from repository
        return await this.repository.find({postId});
    }


    /**
     * Get all comments in a post
     *
     * @param {string} postId
     * @return {Promise<any>}
     */
    public async getCommentsOnPost(postId: string) {
        const post = await this.repository.findOne({postId});

        return await this.commentRepository.getCommentsByPost(post._id);
    }


    public async createCommentOnPost(userId: string, postId: string, body: CreateCommentDto) {
        const post = await this.repository.findOne({postId});

        //clean data
        let doc = {
            user: new mongoose.Types.ObjectId(userId),
            post:  new mongoose.Types.ObjectId(post._id),
            content : body.content
        };

        //save to repository
        const comment = await this.commentRepository.create(doc);

        // get the comment full object
        return this.commentRepository.getCommentById(comment._id);
    }
}
