import {IsDefined, IsJSON, IsMongoId, IsNumber, IsOptional, IsString} from "class-validator";


export class CreatePostDto{
    @IsString()
    title: string;

    @IsMongoId()
    @IsOptional()
    category: string;

    @IsDefined()
    content: object;

    @IsNumber()
    readTime: number;

    @IsString()
    @IsOptional()
    summary: string;
}


export class CreatePostContentDto {
    @IsString()
    html: string;

    @IsJSON()
    content: object;

}


export class PostIdDto {
    @IsString()
    postId: string;
}

export class QueryPostIdDto {
    @IsOptional()
    @IsString()
    slug: string;
}

