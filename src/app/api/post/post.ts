import {User} from "../user/user.interface";

export interface Post {
    author: User;
    postId: string;
    title: string;
    slug: string;
    url: string;
    category: {name: string};
    summary: string;
    content: PostContent;
    createdAt: string;
}


export interface PostContent {
    html: string;
    raw: string;
}

