import {forwardRef, Module} from "@nestjs/common";
import {CoreModule} from "../../core/core.module";
import {UserModule} from "../user/user.module";
import {PostRepository} from "./post.repository";
import {PostModel} from "./post.model";
import {PostService} from "./post.service";
import {PostController} from "./post.controller";
import {CommentModule} from "../comment/coment.module";
import SEED from "./post.seed";
@Module({
    modules: [CoreModule, forwardRef(() => UserModule), CommentModule],
    components: [PostService, PostRepository, PostModel],
    controllers: [PostController],
    exports: [PostService]
})
export class PostModule{
    constructor(private readonly repository:PostRepository) {
        this.repository.ensureSeedData(SEED);
    }
}
