import {Body, Controller, HttpStatus, Post, UseGuards, Req, Put, Param, Delete, Get, Query} from "@nestjs/common";
import {ApiGuard} from "../api.guard";
import {User} from "../user/user.interface";
import {Utils} from "../../core/helpers/utils";
import {CreatePostDto, PostIdDto, QueryPostIdDto} from "./post.dto";
import {PostService} from "./post.service";
import {CreateCommentDto} from "../comment/content.dto";

@Controller('/post')
export class PostController{

    constructor(private readonly service: PostService){}

    @Post("/")
    @UseGuards(ApiGuard)
    public async createPost(@Req() req, @Body() params: CreatePostDto) {

        const user: User = req.locals.user;

        let post = await <any>this.service.create(user._id, params);
        post  = Object.assign({}, post.toJSON(), {author: user});

        return {
            code: HttpStatus.CREATED,
            message : "Successfully Created",
            body : {post : Utils.selectKeys(post, 'postId content title slug author createdAt')}
        };
    }


    @Get("/:postId")
    public async getPost(@Param() params) {

        await this.service.getPost(params.postId);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }

    @Get("/:postId/comment")
    public async getCommentsOnPost(@Param() params) {

        const comments = await this.service.getCommentsOnPost(params.postId);

        return {code: HttpStatus.OK, message : "Request Successful", body: {comments}};
    }

    @Post("/:postId/comment")
    @UseGuards(ApiGuard)
    public async createCommentOnPost(@Req() req, @Param() params: PostIdDto, @Body() body: CreateCommentDto) {

        const user: User = req.locals.user;

        const comment = await this.service.createCommentOnPost(user._id, params.postId, body);

        return {code: HttpStatus.CREATED, message : "Request Successful", body: {comment}};
    }



    @Put("/:postId")
    @UseGuards(ApiGuard)
    public async edit(@Req() req, @Body() body: CreatePostDto, @Param() params: PostIdDto) {

        const user: User = req.locals.user;

        const post = await this.service.edit(user._id, params.postId, body);

        return {
            code: HttpStatus.OK, message: "Request Successful",
            body: {post: Utils.selectKeys(post, 'postId content title slug author createdAt')}
        };
    }

    @Delete("/:postId")
    public async deletePost(@Param() params: PostIdDto) {

        await this.service.delete(params.postId);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }


    @Get("/")
    public async getLatest(@Req() req, @Query() query: QueryPostIdDto) {

        const posts = await this.service.getLatestPosts(query);

        return {
            code: HttpStatus.OK, message : "Request Successful", body : { posts} };
    }
}
