import {IsMongoId, IsString} from "class-validator";

export class CreateCategoryDto{
    @IsString()
    name: string;

    @IsString()
    description: string
}


export class CategoryIdDto {
    @IsMongoId()
    categoryId: string;
}
