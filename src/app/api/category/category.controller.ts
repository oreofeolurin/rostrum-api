import {Body, Controller, HttpStatus, Post, Req, Put, Param, Delete, Get} from "@nestjs/common";
import {Utils} from "../../core/helpers/utils";
import {CategoryService} from "./category.service";
import {CategoryIdDto, CreateCategoryDto} from "./category.dto";

@Controller('/category')
export class CategoryController{

    constructor(private readonly service: CategoryService){}

    @Post("/")
    public async create(@Req() req, @Body() params: CreateCategoryDto) {

        const category = await <any>this.service.create( params);

        return {
            code: HttpStatus.CREATED,
            message : "Successfully Created",
            body : {category : Utils.selectKeys(category, '_id name description createdAt')}
        };
    }


    @Get("/:categoryId")
    public async get(@Param() params: CategoryIdDto) {

        const category = await this.service.getCategory(params.categoryId);

        return {code: HttpStatus.OK, message : "Request Successful", body : Utils.selectKeys(category, "_id name description createdAt")};
    }


    @Put("/:categoryId")
    public async edit(@Body() body: CreateCategoryDto, @Param() params: CategoryIdDto) {

        await this.service.edit(params.categoryId, body);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }

    @Delete("/:categoryId")
    public async delete(@Param() params) {

        await this.service.delete(params.categoryId);

        return {code: HttpStatus.OK, message : "Request Successful"};
    }

    @Get("/")
    public async getCategories(@Req() req) {

        const categories = await this.service.getAllCategories();

        return {
            code: HttpStatus.OK, message : "Request Successful", body : { categories} };
    }
}
