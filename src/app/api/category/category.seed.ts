export default [
    {
        "_id": "5a920e606a60863dac527900", "updatedAt": "2018-02-25T01:16:16.137Z",
        "createdAt": "2018-02-25T01:16:16.137Z",
        "name": "Technology", "description": "Post about the technological world", "__v": 0
    },
    {
        "_id": "5a920ea109494715709e218b",
        "updatedAt": "2018-02-25T01:24:43.186Z",
        "createdAt": "2018-02-25T01:17:21.147Z",
        "name": "Fashion",
        "description": "Post about the fashion and glamor world",
        "__v": 0
    },
    {
        "_id": "5a920ec909494715709e218c",
        "updatedAt": "2018-02-25T01:18:01.250Z",
        "createdAt": "2018-02-25T01:18:01.250Z",
        "name": "Education",
        "description": "Post about the teaching and education",
        "__v": 0
    },
    {
        "_id": "5a920eda09494715709e218d",
        "updatedAt": "2018-02-25T01:18:18.157Z",
        "createdAt": "2018-02-25T01:18:18.157Z",
        "name": "Finnace",
        "description": "Post about the finnace world",
        "__v": 0
    },
    {
        "_id": "5a920eed09494715709e218e",
        "updatedAt": "2018-02-25T01:18:37.950Z",
        "createdAt": "2018-02-25T01:18:37.950Z",
        "name": "Humman Science",
        "description": "Post about the human world",
        "__v": 0
    },
    {
        "_id": "5a920f0309494715709e218f",
        "updatedAt": "2018-02-25T01:18:59.646Z",
        "createdAt": "2018-02-25T01:18:59.646Z",
        "name": "Politics",
        "description": "Post about the world view poiliticatilly",
        "__v": 0
    }];
