import {Component} from "@nestjs/common";
import {CreateCategoryDto} from "./category.dto";
import {CategoryRepository} from "./category.repository";

@Component()
export class CategoryService{

    constructor(private readonly repository: CategoryRepository){}


    public async create(body: CreateCategoryDto): Promise<object>{

        //clean data
        const details = {
            name : body.name.trim(),
            description: body.description.trim()
        };

        //save to repository
        return await this.repository.create(details);

    }


    public async edit(categoryId: string, body:  CreateCategoryDto){

        //clean data
        let updateObject = {
            name : body.name.trim(),
            description: body.description.trim()
        };

        //save to repository
        return await this.repository.updateById(categoryId, updateObject);

    }

    public async delete(categoryId: string){

        //delete from this.repository
        return await this.repository.deleteById(categoryId);

    }

    public async getAllCategories(){
        //get from repository
        return await this.repository.find({}, "_id name description createdAt");

    }

    public async getCategory(id: string){
        //get from repository
        return await this.repository.findOneById(id);

    }

}
