import {Module} from "@nestjs/common";
import {CoreModule} from "../../core/core.module";
import {CategoryService} from "./category.service";
import {CategoryModel} from "./category.model";
import {CategoryRepository} from "./category.repository";
import {CategoryController} from "./category.controller";
import SEED from "./category.seed";

@Module({
    modules: [CoreModule],
    controllers: [CategoryController],
    components: [CategoryService, CategoryRepository, CategoryModel],
    exports: [ CategoryService ]
})
export class CategoryModule {
    constructor(private readonly repository: CategoryRepository) {
        this.repository.ensureSeedData(SEED);
    }
}
