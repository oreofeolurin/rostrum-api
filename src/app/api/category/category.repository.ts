import {Component} from "@nestjs/common";
import {Repository} from "../../core/abstracts/repository";
import {CategoryModel} from "./category.model";

@Component()
export class CategoryRepository  extends Repository {

    constructor(model: CategoryModel) {
        super(model);
    }
}
