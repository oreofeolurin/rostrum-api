import * as mongoose from 'mongoose';
import {Component} from "@nestjs/common";
import {MongodbHandle} from "../../core/connections/mongodb-handle";

@Component()
export class CategoryModel {
    private model: mongoose.Model<mongoose.Document>;

    constructor(private readonly mongodb: MongodbHandle) {
        this.registerModel();
    }

    /**
     * Gets the mongoose Document
     *
     * @return {mongoose.Model<mongoose.Document>}
     */
    public getModel(): mongoose.Model<mongoose.Document> {
        return this.model;
    }

    /**
     * Creates the mongoose model
     */
    private registerModel() {
        //create the Model
        let model = CategoryModel.createModel(this.mongodb.getHandle());
        this.setModel(model);
    }

    /**
     * Set the mongoose model
     */
    public setModel(model: mongoose.Model<mongoose.Document>) {
        this.model = model;
    }


    /**
     * Create the mongoose model using the DBConnection
     *
     * @param DBConnection
     * @return {mongoose.Model<mongoose.Document>}
     */
    public static createModel(DBConnection: mongoose.Connection): mongoose.Model<mongoose.Document> {

        //create schema
        let schema = this.createSchema();

        return DBConnection.model('Category', schema);


    }

    private static createSchema(): mongoose.Schema {

        let schema = new mongoose.Schema({
            name: {
                type: String,
                unique: true,
                required: "Category Name is Required",
            },

            description:  {
                type: String,
                required: "Category Description is Required",
            },

        }, {'timestamps': true});

        schema.set('toJSON', {getters: true, virtuals: true});

        return schema;

    }

}
