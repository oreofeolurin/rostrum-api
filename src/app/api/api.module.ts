import {Module} from "@nestjs/common";
import {CategoryModule} from "./category/category.module";
import {PostModule} from "./post/post.module";

@Module({
    modules: [CategoryModule, PostModule]
})
export class ApiModule{}
