import * as mongoose from 'mongoose';
import {Component} from "@nestjs/common";
import {MongodbHandle} from "../../core/connections/mongodb-handle";
import {User} from "../user/user.interface";

@Component()
export class CommentModel {
    private model: mongoose.Model<mongoose.Document>;

    constructor(private readonly mongodb: MongodbHandle) {
        this.registerModel();
    }

    /**
     * Gets the mongoose Document
     *
     * @return {mongoose.Model<mongoose.Document>}
     */
    public getModel(): mongoose.Model<mongoose.Document> {
        return this.model;
    }

    /**
     * Creates the mongoose model
     */
    private registerModel() {
        //create the Model
        let model = CommentModel.createModel(this.mongodb.getHandle());
        this.setModel(model);
    }

    /**
     * Set the mongoose model
     */
    public setModel(model: mongoose.Model<mongoose.Document>) {
        this.model = model;
    }


    /**
     * Create the mongoose model using the DBConnection
     *
     * @param DBConnection
     * @return {mongoose.Model<mongoose.Document>}
     */
    public static createModel(DBConnection: mongoose.Connection): mongoose.Model<mongoose.Document> {

        //create schema
        let schema = this.createSchema();

        return DBConnection.model('Comment', schema);


    }

    private static createSchema(): mongoose.Schema {

        let schema = new mongoose.Schema({
            user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },

            post: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Post',
                required: true
            },

            content: PostContentModel.createSchema() as any,

        }, {'timestamps': true});

        schema.set('toJSON', { getters: true, transform: (doc, ret) =>{
                delete ret.id;
                return ret;
            }});

        return schema;

    }

}


class PostContentModel{

    public static createSchema(){
        return  new mongoose.Schema({
            html  : String ,
            raw : Object
        });
    };

}
