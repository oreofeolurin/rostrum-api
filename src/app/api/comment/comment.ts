import {User} from "../user/user.interface";

export interface Post {
    user: User;
    postId: string;
    content: RosContent;
    createdAt: string;
}


export interface RosContent {
    html: string;
    raw: string;
}

