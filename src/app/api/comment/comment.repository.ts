import {Component} from "@nestjs/common";
import {Repository} from "../../core/abstracts/repository";
import * as mongoose from "mongoose";
import {CommentModel} from "./comment.model";

@Component()
export class CommentRepository extends Repository{

    constructor(model: CommentModel) {
        super(model);
    }


    /**
     * Gets all document in the database
     *
     * @return {Promise<any>}
     * @param id
     */
    public async getCommentById(id: string){

        return await this.findOneWithPopulate({_id: id}, 'user content createdAt', {path: 'user', select: 'username name summary'})

    }


    /**
     *
     * @param id Post Mongoose ID
     * @return {Promise<any>}
     */
    public async getCommentsByPost(id: mongoose.Types.ObjectId) {

        return await this.findWithPopulate({post: id}, 'user content createdAt', {path: 'user', select: 'username name summary'})

    }

}
