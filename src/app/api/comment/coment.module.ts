import {Module} from "@nestjs/common";
import {CoreModule} from "../../core/core.module";
import {CommentRepository} from "./comment.repository";
import {CommentModel} from "./comment.model";
import SEED from "./comment.seed";
@Module({
    modules: [CoreModule],
    components: [CommentRepository, CommentModel],
    exports: [CommentRepository]
})
export class CommentModule{
    constructor(private readonly repository: CommentRepository) {
        this.repository.ensureSeedData(SEED);
    }
}
