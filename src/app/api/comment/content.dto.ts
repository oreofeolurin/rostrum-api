import {IsDefined} from "class-validator";


export class CreateCommentDto{
    @IsDefined()
    content: object;
}
