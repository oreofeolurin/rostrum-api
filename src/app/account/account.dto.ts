import {IsAlphanumeric, IsEmail, IsOptional, IsString, Matches, MinLength} from "class-validator";

export class CreateUserDto {
    @IsString()
    readonly name: string;

    @IsEmail()
    readonly email: string;

    @IsString()
    @MinLength(8)
    readonly password: string;

    @Matches(/^(?![0-9])\w{3,}/)
    @IsAlphanumeric()
    readonly username: string;
}


export class CheckUserAvailabilityDto {

    @IsEmail()
    @IsOptional()
    readonly email: string;

    @Matches(/^(?![0-9])\w+/)
    @IsAlphanumeric()
    @IsOptional()
    readonly username: string;

}

export class CredentialDto{
    @IsEmail()
    readonly email: string;

    @IsString()
    @MinLength(8)
    readonly password: string;

}
