import {Controller, Post, Body, HttpStatus, Query, Get,} from '@nestjs/common';
import {CheckUserAvailabilityDto, CreateUserDto, CredentialDto} from './account.dto';
import {AccountService} from "./account.service";

@Controller('/account')
export class AccountController {

    constructor(private readonly accountService: AccountService){}

    @Post("/create/user")
    public async createUser(@Body() createUser:  CreateUserDto) {
        try{
            //lets create the account
            await this.accountService.create(createUser);

            return {code: HttpStatus.CREATED, message : "Successfully Created"};

        }catch (err){ throw err; }
    }

    @Get("/check/user/availability")
    public async checkUserAvailability(@Query() query:  CheckUserAvailabilityDto) {
        try{
            //lets create the account
            await this.accountService.checkUserAvailability(query);

            return {code: HttpStatus.OK, message : "Availability is true"};

        }catch (err){throw err; }
    }

    /**
     * Creates User JWT Token to be used for authenticating server calls
     *
     * @param params
     */
    @Post("/create/user/token")
    public async createUserToken(@Body() params: CredentialDto){

        try{
            let {user , token}  = await this.accountService.authenticateCredentials(params);

            return {code: HttpStatus.OK, message : "Request Successful", body: {user , token} };

        }catch (err){ throw err; }

    }

}
