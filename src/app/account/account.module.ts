import {Module} from "@nestjs/common";
import {CoreModule} from "../core/core.module";
import {AccountController} from "./account.controller";
import {AccountService} from "./account.service";
import {UserModule} from "../api/user/user.module";

@Module({
    modules: [CoreModule, UserModule],
    controllers: [AccountController],
    components: [AccountService]
})
export class AccountModule{}
