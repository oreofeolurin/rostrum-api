import {Component, HttpStatus, Post, Query} from '@nestjs/common';
import {CheckUserAvailabilityDto, CreateUserDto, CredentialDto} from "./account.dto";
import * as jwt from 'jsonwebtoken';
import {UserRepository} from "../api/user/user.repository";
import {Utils} from "../core/helpers/utils";
import {AppException} from "../core/exceptions/app.exception";

@Component()
export class AccountService {

    constructor(private readonly repository: UserRepository){}

    public async create(options:  CreateUserDto) {

        //clean data
        let userDetails = {
            email: options.email.trim().toLowerCase(),
            username: options.username.trim(),
            password: options.password.trim(),
            name: options.name.trim(),
        };

        //save to repository
        await this.repository.createUser(userDetails);
    }


    public async checkUserAvailability(query:  CheckUserAvailabilityDto) {

        const conditions = Utils.removeNilValues({email: query.email, username: query.username});
        const users = await this.repository.find(conditions) as Array<object>;
        if (users.length > 0) throw (AppException.USER_EXIST);
    }



    public async authenticateCredentials(params : CredentialDto){

        //lets create the credentials
        let email = params["email"].toLowerCase().trim();
        let password =  params["password"].trim();

        let user = await this.repository.findUserByCredential(email, password);
        let signature =  {user : {_id : user["_id"]}};

        //lets create the token
        const token = jwt.sign(signature, process.env.JWT_SECRET, {
            expiresIn: "30d",
            issuer : "rostrum:rostrum-api"
        });

        return {user, token};

    }

}
