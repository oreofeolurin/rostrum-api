# Rostrum (rostrum-api)

A forum for sharing ideas

### Introduction
This is the API Backend for thr Rostrum APP. It was Built using the NestJS Framework, which is a fast and effecient way of building apps that scale.

### Steps to run
- Run install  - `$ npm install` to get neccessary files
- Ensure you have mongodb insttalled on your PC, if not you can switch the `MONGODB_URI` in the `.env` to point to a `MONGODB_URI_REMOTE` mongodb resource.
- Run `$ npm start`

##### Seed Data
The application has a seed data generator to help boostrap some data into mongodb. The seed data contains two users which could be used to test login on the frontend app. Here are the login details

1. Email - johndoe@mail.com, Password -  password
2. Email - edwinclark@mail.com, Pasword- password

The seed data also contains data for the `post` and `category` models.
